<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('doctors')->insert([
            [
                'cid' => '11807001111',
                'email' => 'tenzin@gmail.com',
                'name' => 'Tenzin',
                'phone' => '17701111',
                'gender' => 'male',
                'specialization' => 'Cardiologist',
                'location' => 'Thimphu',
                'hospitalName' => 'JDWNRH',
                'description' => 'Dermatologists specialize in diagnosing and treating conditions related to the skin, hair, and nails. This can range from common issues like acne and eczema to more serious conditions like skin cancer.',
                'image' => Null,
            ],
            [
                'cid' => '11807002222',
                'email' => 'dorji@gmail.com',
                'name' => 'Dorji',
                'phone' => '17702222',
                'gender' => 'male',
                'specialization' => 'Dermatologist',
                'location' => 'Thimphu',
                'hospitalName' => 'JDWNRH',
                'description' => 'A cardiologist is a doctor who specializes in the diagnosis, treatment, and prevention of diseases and conditions related to the cardiovascular system, including the heart and blood vessels.',
                'image' => Null,
            ],
            [
                'cid' => '11807003333',
                'email' => 'yangso@gmail.com',
                'name' => 'Yangso',
                'phone' => '17703333',
                'gender' => 'female',
                'specialization' => 'Neurologist',
                'location' => 'Paro',
                'hospitalName' => 'JDWNRH2',
                'description' => 'Neurologists focus on diagnosing and treating disorders of the nervous system, including the brain, spinal cord, and nerves. Conditions they deal with may include stroke, epilepsy, Alzheimer disease, and multiple sclerosis.',
                'image' => Null,
            ],
            [
                'cid' => '11807004444',
                'email' => 'dechen@gmail.com',
                'name' => 'Dechen',
                'phone' => '17704444',
                'gender' => 'female',
                'specialization' => 'Psychiatrist',
                'location' => 'Mongar',
                'hospitalName' => 'JDWNRH3',
                'description' => 'Psychiatrists are medical doctors who specialize in the diagnosis, treatment, and prevention of mental illnesses and disorders. They are trained to assess both the mental and physical aspects of psychological problems and can prescribe medication as well as provide therapy.',
                'image' => Null,
            ],
            [
                'cid' => '11807005555',
                'email' => 'subba@gmail.com',
                'name' => 'Subba',
                'phone' => '17705555',
                'gender' => 'male',
                'specialization' => 'Oncologist',
                'location' => 'Tsirang',
                'hospitalName' => 'JDWNRH4',
                'description' => 'Oncologists specialize in the diagnosis and treatment of cancer. They may specialize further in areas like medical oncology (using medications like chemotherapy), radiation oncology (using radiation therapy), or surgical oncology (performing cancer surgeries).',
                'image' => Null,
            ],
        ]);
    }
}
