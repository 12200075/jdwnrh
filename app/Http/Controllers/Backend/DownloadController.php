<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hospitals;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class DownloadController extends Controller
{
    public function downloadHospital(Request $request){
        $hospitals = DB::table('hospitals')->select('id', 'name', 'location',)->get();

        if ($hospitals->isEmpty()) {
            return redirect()->back()->with('error', 'No hospitals were found in the database.');
        }

        $csvFileName = 'Hospital.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $hospitals[0]);
        fputcsv($handle, $header);

        foreach ($hospitals as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Hospital.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    public function downloadDoctor(Request $request){
        $doctors = DB::table('doctors')->select( 'cid', 'email', 'name', 'phone', 'specialization', 'gender', 'location','hospitalName','description',)->get();

        if ($doctors->isEmpty()) {
            return redirect()->back()->with('error', 'No Doctors were found in the database.');
        }

        $csvFileName = 'Doctor.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $doctors[0]);
        fputcsv($handle, $header);

        foreach ($doctors as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Doctor.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }


    // Download Director
    public function downloadDirector(Request $request){
        $users = DB::table('users')->select('cid', 'name', 'email','phone') ->where('role', 'director')->get();

        if ($users->isEmpty()) {
            return redirect()->back()->with('error', 'No Directors were found in the database.');
        }

        $csvFileName = 'Director.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $users[0]);
        fputcsv($handle, $header);

        foreach ($users as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Director.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    // Download Adm
    public function downloadAdm(Request $request){
        $users = DB::table('users')->select('cid', 'name', 'email','phone') ->where('role', 'adm')->get();

        if ($users->isEmpty()) {
            return redirect()->back()->with('error', 'No Administrative Assistants were found in the database.');
        }

        $csvFileName = 'ADM.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $users[0]);
        fputcsv($handle, $header);

        foreach ($users as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'ADM.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    ///////////////////////////////////////////////////// Administrative Assistant //////////////////////////////////////////////////////
    // Administrative Assistant download doctor
    public function admDownloadDoctor(Request $request){
        $user = Auth::user();

        $doctors = DB::table('doctors')
                    ->where('location', $user->location)
                    ->where('hospitalName', $user->hospitalName)
                    ->select('cid', 'email', 'name', 'phone', 'specialization', 'gender', 'location','hospitalName','description')
                    ->get();

        if ($doctors->isEmpty()) {
            return redirect()->back()->with('error', 'No doctors were found in the database for your location and hospital.');
        }

        $csvFileName = 'Doctor.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $doctors[0]);
        fputcsv($handle, $header);

        foreach ($doctors as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file
        return response($content, 200, $headers);
    }

     // Administrative Assistant Download Leave
     public function downloadLeave(Request $request){
        $user = Auth::user();

        $leaves = DB::table('leaves')
                    ->where('location', $user->location)
                    ->where('hospitalName', $user->hospitalName)
                    ->select('cid', 'name', 'start','end', 'type', 'location', 'hospitalName', 'remarks')
                    ->get();

        if ($leaves->isEmpty()) {
            return redirect()->back()->with('error', 'No leaves were found in the database for your location and hospital.');
        }

        $csvFileName = 'Leave.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $leaves[0]);
        fputcsv($handle, $header);

        foreach ($leaves as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file
        return response($content, 200, $headers);
    }

    public function downloadUserLeave(Request $request, $cid){
        $leaves = DB::table('leaves')
            ->where('cid', $cid)
            ->select('start', 'end', 'type', 'remarks')
            ->get();

        if ($leaves->isEmpty()) {
            return redirect()->back()->with('error', 'No leaves were found in the database for the user.');
        }

        // Get the user's name based on the $cid (you may need to modify this depending on your data structure)
        $userName = DB::table('leaves')->where('cid', $cid)->value('name');

        $title = 'Leave Log of ' . $userName;

        $csvFileName = 'Leave_Log_' . $userName . '.csv';

        $handle = fopen('php://memory', 'r+');

        // Write the title to CSV
        fputcsv($handle, [$title]);

        // Write CSV header
        fputcsv($handle, ['Start Date', 'End Date', 'Leave Type', 'Remarks']);

        // Write leave records to CSV
        foreach ($leaves as $leave) {
            fputcsv($handle, [$leave->start, $leave->end, $leave->type, $leave->remarks]);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file
        return response($content, 200, $headers);
    }

    // Director download doctor
    // public function admDownloadDoctor(Request $request){
    //     $user = Auth::user();

    //     $doctors = DB::table('doctors')
    //                 ->where('location', $user->location)
    //                 ->where('hospitalName', $user->hospitalName)
    //                 ->select('cid', 'email', 'name', 'phone', 'specialization', 'gender', 'location','hospitalName','description')
    //                 ->get();

    //     if ($doctors->isEmpty()) {
    //         return redirect()->back()->with('error', 'No doctors were found in the database for your location and hospital.');
    //     }

    //     $csvFileName = 'Doctor.csv';

    //     $handle = fopen('php://memory', 'r+');

    //     $header = array_keys((array) $doctors[0]);
    //     fputcsv($handle, $header);

    //     foreach ($doctors as $row) {
    //         unset($row->created_at, $row->updated_at);
    //         fputcsv($handle, (array) $row);
    //     }

    //     rewind($handle);

    //     $content = stream_get_contents($handle);
    //     fclose($handle);

    //     $headers = [
    //         'Content-Type' => 'text/csv',
    //         'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
    //     ];

    //     // Return CSV file
    //     return response($content, 200, $headers);
    // }

}
