<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctors;
use App\Models\Leaves;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\User;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;
use App\Models\Hospitals;

class DirectorController extends Controller
{
    public function directorDashboard(){
        $recent = Leaves::all();
        $data = DB::table('leaves')
            ->selectRaw('MONTH(start) as month, COUNT(*) as count, type')
            ->groupBy('month', 'type')
            ->get();
        return view('director.dashboard', [
            'recent' => $recent,
            'data' => $data,
        ]);
    }

     //Profile
     public function directorProfile(){
        $user = Auth::user();
        return view('director.directorProfile',  compact('user'));
    }

    public function directorEditProfile($cid){
        $users = User::where('cid', $cid)->first();

        return view('director.directorEditProfile', ['user' => $users]);
    }

    public function directorUpdateProfile(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'User not found.');
        }

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        $user->save();

        return redirect()->route('directorProfile')->with('success', 'User updated successfully.');
    }

    public function directorResetPassword($cid){
        $users = User::where('cid', $cid)->first();

        return view('director.directorResetPassword', ['user' => $users]);
    }

    // public function admUpdatePassword(Request $request, $cid) {
    //     $user = User::find($cid);

    //     if (!$user) {
    //         return redirect()->back()->with('error', 'User not found.');
    //     }

    //     // Validate the request data
    //     $request->validate([
    //         'current_password' => 'required|string',
    //         'new_password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);

    //     // Check if the provided current password matches the user's actual password
    //     if (!Hash::check($request->current_password, $user->password)) {
    //         return redirect()->back()->with('error', 'Current password is incorrect.');
    //     }

    //     // Update the user's password
    //     $user->password = Hash::make($request->new_password);
    //     $user->save();

    //     return redirect()->route('directorProfile')->with('success', 'Password updated successfully.');
    // }

    //Display User
    public function directorDoctor(){
        $hospitals = Hospitals::all();

        return view('director.directorViewDoctor', compact('hospitals'));
    }

    public function directorViewDoctorList($hospitalName){

        $user = Auth::user();
        $leaves = Leaves::all();

        $doctors = Doctors::where('hospitalName', $hospitalName)->get();

        $doctorStatus = [];

        foreach ($doctors as $doctor) {
            $latestLeave = Leaves::where('cid', $doctor->cid)
                                 ->orderBy('start', 'desc')
                                 ->first();

            if ($latestLeave) {
                // If a leave entry exists
                $currentDate = date('Y-m-d');
                if ($currentDate >= $latestLeave->start && $currentDate <= $latestLeave->end) {
                    // Current date is within leave period
                    $status = 'Not available';
                } else {
                    // Current date is not within leave period
                    $status = 'Available';
                }
            } else {
                // Doctor's CID is not present in leaves table
                $status = 'Available';
            }

            $doctorStatus[$doctor->cid] = $status;
        }

        return view('director.directorViewDoctorList', ['doctors' => $doctors, 'leaves' => $leaves, 'doctorStatus' => $doctorStatus]);
    }

    public function directorDoctorDetail($cid){
        $doctor = Doctors::where('cid', $cid)->first();

        return view('director.directorDoctorDetail', ['doctor' => $doctor]);
    }


    // Chart
    public function getDoctorsOnLeave(Request $request)
        {
            $month = $request->input('month');
            $leaveType = $request->input('leaveType');

            $doctorsOnLeave = DB::table('leaves')
                ->whereMonth('start', $month)
                ->where('type', $leaveType)
                ->select('name', 'hospitalName', 'cid')
                ->get();

            return response()->json($doctorsOnLeave);
        }

        public function directorLeaveLog($cid, $type = null){
            if ($type) {
                $leave = Leaves::where('cid', $cid)
                            ->where('type', $type)
                            ->get();
            } else {
                $leave = Leaves::where('cid', $cid)->get();
            }
            return view('director.leaveLog', ['leave' => $leave, 'cid' => $cid]);
        }
}
