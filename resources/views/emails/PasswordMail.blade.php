<div>
    <p>Your new password is: <strong>{{ $password }}</strong></p>
    <p>Please use this password to log in. For security reasons, we recommend changing your password after logging in.</p>
</div>
