<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/leave.css') }}">
</head>

<style>
    .custom-dropdown {
      font-size: 14px;
      display: flex;
      align-items: center;
      color: rgb(86, 84, 84);
      margin-top: 20px;
    }

    .custom-dropdown select {
      border: 1px solid gray;
      padding: 5px 42px;
      margin-left: 20px;
      border-radius: 5px;
      text-decoration: none;
      color: gray;
      font-weight: 400;
    }
  </style>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto  active"><i class="fas fa-calendar-alt"></i> <span>Leave</span></a></li>
            <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>

<main id="main">
    <section id="hero" class="about">
    <div class="container text-left">
        <div class="d-flex justify-content-between align-items-center " style="margin: 60px 0px;">
        <h4 style="font-weight:800; margin: 0;">Doctor Leave </h4>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger" id="error-alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <script>
            document.addEventListener("DOMContentLoaded", function() {
                const errorAlert = document.getElementById('error-alert');
                if (errorAlert) {
                    setTimeout(function() {
                        errorAlert.style.display = 'none';
                    }, 5000);
                }
            });
        </script>

        <form method="POST" action="{{ route('admAddLeave') }}" enctype="multipart/form-data" id="leaveForm">
            {!! csrf_field() !!}
            <div class="dropdown d-lg-flex d-block align-items-center">
                <h6>Choose Doctor:</h6>
                <select class="dropdown-toggle" aria-expanded="false" name="cid" id="cid">
                    <option value="">-- Select Doctor --</option>
                    <?php
                    $user = Auth::user();
                    $doctors = DB::table('doctors')->where('hospitalName', $user->hospitalName)->get();
                    if ($doctors->count() > 0) {
                        foreach ($doctors as $doctor) {
                            echo '<option value="' . $doctor->cid . '">' . $doctor->name . ' (' . $doctor->cid . ')</option>';
                        }
                    } else {
                        echo '<option disabled>No doctors available</option>';
                    }
                    ?>
                </select>
            </div>
            <div id="cidError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>

            <div class="form-group" style="margin-top: 40px; font-size: 14px;">
                <p>LEAVE TYPE:</p>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="leave" name="type" value="leave" checked>
                    <label class="form-check-label" for="leave">Leave</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="tour" name="type" value="tour">
                    <label class="form-check-label" for="tour">Tour</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="event" name="type" value="event">
                    <label class="form-check-label" for="event">Event Participant</label>
                </div>
                <div id="typeError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>
            </div>

            <div class="date col-lg-8" style="margin-top: 40px;">
                <div class="col-lg-12 d-lg-flex d-block">
                    <div class="col-6">
                        <label>From:</label>
                        <input type="date" class="form-control" id="start" name="start" min="{{ date('Y-m-d') }}">
                        <div id="startError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>
                    </div>
                    <div class="col-6 mx-lg-4">
                        <label>To:</label>
                        <input type="date" class="form-control" id="end" name="end" min="{{ date('Y-m-d') }}">
                        <div id="endError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>
                    </div>
                </div>
            </div>

            <div class="form-group" style="margin-top: 40px; width: 666px">
                <label for="remarks">Remarks</label>
                <textarea class="form-control" id="remarks" name="remarks" rows="3"></textarea>
                <div id="remarksError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>
            </div>

            <input type="file" name="image" id="image" class="form-control mt-4" style="font-size: 14px;width: 250px;">
            <div id="imageError" class="error-message" style="color: red; font-size: 14px; font-weight: 600; margin-top: 5px;"></div>

            <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="{{ route('admViewLeave') }}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>

    </div>
    </section>
</main>
</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    document.getElementById('start').addEventListener('change', function() {
        var startDate = new Date(this.value);
        var endDateInput = document.getElementById('end');
        var endDate = new Date(endDateInput.value);

        if (startDate > endDate) {
            endDateInput.value = this.value;
        }
        endDateInput.min = this.value;
    });

    document.getElementById('end').addEventListener('change', function() {
        var endDate = new Date(this.value);
        var startDateInput = document.getElementById('start');
        var startDate = new Date(startDateInput.value);

        if (endDate < startDate) {
            alert('End date cannot be before start date.');
            this.value = startDateInput.value;
        }
    });
</script>

<script>
    document.getElementById('leaveForm').addEventListener('submit', function(event) {
    let isValid = true;

    document.querySelectorAll('.error-message').forEach(function(error) {
        error.textContent = '';
    });

    let cid = document.getElementById('cid').value;
    if (!cid) {
        isValid = false;
        document.getElementById('cidError').textContent = 'Please select a doctor.';
    }

    // Validate Leave Type (at least one should be checked)
    let leaveTypeChecked = document.querySelector('input[name="type"]:checked');
    if (!leaveTypeChecked) {
        isValid = false;
        document.getElementById('typeError').textContent = 'Please select a leave type.';
    }

    // Validate Date fields
    let startDate = document.getElementById('start').value;
    let endDate = document.getElementById('end').value;
    if (!startDate) {
        isValid = false;
        document.getElementById('startError').textContent = 'Please select a start date.';
    }
    if (!endDate) {
        isValid = false;
        document.getElementById('endError').textContent = 'Please select an end date.';
    }
    if (startDate && endDate && new Date(startDate) > new Date(endDate)) {
        isValid = false;
        document.getElementById('endError').textContent = 'End date must be after start date.';
    }

    // Validate Remarks
    let remarks = document.getElementById('remarks').value;
    if (remarks.trim() === '') {
        isValid = false;
        document.getElementById('remarksError').textContent = 'Please provide remarks.';
    }

    // Validate File upload (optional, if required)
    let image = document.getElementById('image').files[0];
    if (!image) {
        isValid = false;
        document.getElementById('imageError').textContent = 'Please upload an image.';
    }

    // Prevent form submission if any validation fails
    if (!isValid) {
        event.preventDefault();
    }
});

  </script>
</html>
