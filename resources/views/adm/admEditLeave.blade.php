<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/adm/admViewleave') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto  active"><i class="fas fa-calendar-alt"></i> <span>Leave</span></a></li>
            <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>

  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800; margin: 50px 0px;">Edit Leave</h4>

    <form method="POST" action="{{ route('admUpdateLeave', ['id' => $leave->id]) }}" style="margin-top: 20px;" accept-charset="UTF-8" enctype="multipart/form-data">
        @method('PUT')
        @csrf

        @if(session('success'))
            <div class="alert alert-success" id="successAlert">
                {{ session('success') }}
            </div>
            <script>
                setTimeout(function() {
                    $('#successAlert').fadeOut('fast');
                }, 5000);
            </script>
        @endif

        @if(session('error'))
            <div id="error-alert" class="alert alert-danger">
                {{ session('error') }}
            </div>
            <script>
                setTimeout(function() {
                    var errorAlert = document.getElementById('error-alert');
                    if(errorAlert) {
                        errorAlert.style.display = 'none';
                    }
                }, 5000);
            </script>
        @endif

            <div class="form-row">
                <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="{{ $leave->name }}" class="form-control rounded-1">
                </div>

                <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                    <label for="cid">CID</label>
                    <input type="text" name="cid" id="cid" value="{{ $leave->cid }}" class="form-control rounded-1" >
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                    <label for="start">Start</label>
                    <input type="date" name="start" id="start" value="{{ $leave->start }}" class="form-control rounded-1" >
                </div>

                <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                    <label for="end">End</label>
                    <input type="date" name="end" id="end" value="{{ $leave->end }}" class="form-control rounded-1" >
                </div>
            </div>

            <div class="form-row">
                <div class="col-12 text-area">
                <label for="remarks">Remarks</label>
                <textarea name="remarks" id="remarks" rows="5" class="form-control rounded-1">{{ $leave->remarks }}</textarea>

                </div>
            </div>

            <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4"  style="font-size: 14px;"/>

            <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
                <button class="btn btn-primary " type="submit">Save</button>
                <a href="{{ route('getLeaveList') }}" class="btn btn-secondary">Cancel</a>
            </div>

    </form>
    </div>
</section>
</main>
  <script src="{{ asset('assets/js/main.js') }}"></script>
</body>
</html>
