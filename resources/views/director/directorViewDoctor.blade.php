<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Hospital.css') }}">
</head>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/director/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/director/directorViewDoctor') }}" class="nav-link scrollto active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/director/directorProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>
<main id="main">
 <section id="hero" class="about">
  <div class="container text-left">

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 20px;">
        <h4 style="font-weight:800; margin: 0;">Hospital List</h4>
          <div class="d-none d-md-flex justify-content-start align-items-center">
              <button class="btn btn-primary" type="button" style="margin-right: 30px;">Download</button>
          </div>
      </div>

      <div class="d-md-none mt-3 pr-md-3" style="margin-bottom: 20px;" >
          <button class="btn btn-primary " type="button">Download</button>
      </div>

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 20px;">
        <h4 style="font-weight:800; margin: 0;">
            <div class="dropdown" style="font-size: 14px; display: flex; align-items: center; color: rgb(86, 84, 84);margin-top: 20px;">
                Select Dzongkhag:
                <select class="dropdown-toggle" style="margin-left:20px; font-size: 14px; display: flex; align-items: center; color: rgb(86, 84, 84);" name="location" id="locationSelect">
                    <option>-- Select Location --</option>
                    <?php
                    $locations = App\Models\Hospitals::pluck('location')->unique();

                    if($locations->count() > 0) {
                        foreach($locations as $location) {
                            echo '<option value="' . $location . '">' . $location . '</option>';
                        }
                    } else {
                        echo '<option disabled>No locations available</option>';
                    }
                    ?>
                </select>
            </div>
        </h4>
    </div>

        <div id="hospitalList">
            @if($hospitals->isNotEmpty())
                @foreach($hospitals as $hospital)
                    <div class="card col-12 locationCard" data-location="{{ $hospital->location }}" onclick="navigateToDetail('{{ route('directorViewDoctorList', ['name' => $hospital->name]) }}')">
                        <img src="{{ asset('/storage/images/' . $hospital->image) }}" alt="{{ $hospital->name }}">
                        <div class="card-body" style="display: flex;flex-direction: row; align-items: center;">
                            <p class="card-text">
                                <b>{{ $hospital->name }}</b><br>
                                {{ $hospital->location }}<br>
                            </p>
                        </div>
                    </div>
                @endforeach
            @else
                <p>No hospitals found.</p>
            @endif
        </div>

</section>
</main>

</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    function navigateToDetail(url) {
        window.location.href = url;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#locationSelect').change(function() {
                var selectedLocation = $(this).val();
                $('.locationCard').each(function() {
                    var location = $(this).data('location');
                    if (selectedLocation === '-- Select Location --' || location === selectedLocation) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            });
        });
    </script>
</html>
