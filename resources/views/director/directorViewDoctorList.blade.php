<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}">
</head>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/director/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/director/directorViewDoctor') }}" class="nav-link scrollto  active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/director/directorProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>

<main id="main">
 <section id="hero" class="about">
  <div class="container text-left">

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 40px;">
        <h4 style="font-weight:800; margin: 0;">Doctor List</h4>
          <div class="d-none d-md-flex justify-content-start align-items-center">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <select class="custom-select" id="searchTypeDesktop">
                                <option value="">Sort</option>
                                <option value="duty">Duty</option>
                                <option value="tour">Tour</option>
                                <option value="leave">Leave</option>
                                <option value="events">Events</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            </div>
      </div>

      <div class="d-md-none mt-3 pr-md-3" style="margin-bottom: 40px;" >
          <div class="col mt-3">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <select class="custom-select" id="searchTypeMobile" style="padding: 4px 10px">
                            <option value="">Sort</option>
                            <option value="duty">Duty</option>
                            <option value="tour">Tour</option>
                            <option value="leave">Leave</option>
                            <option value="events">Events</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
      </div>

      @if($doctors->isEmpty())
                      <p>No doctors found.</p>
                      @else
                      @foreach($doctors as $doctor)
                      <div class="card col-12" style="padding: 10px;">
                          <div style="cursor: pointer; width: 100%; display: flex; flex-direction: row;" onclick="navigateToDetail('{{ route('directorDoctorDetail', ['cid' => $doctor->cid]) }}')">
                              <img src="{{ asset('/storage/images/' . $doctor->image) }}" alt="{{ $doctor->name }}" class="card-img-top" style="width: 100px; height: 100px; margin-right: 10px;">
                              <div class="card-body" style="display: flex; flex-direction: column; justify-content: center;">
                                  <p class="card-text mb-0">Name: <b>{{ $doctor->name }}</b></p>
                                  <p class="card-text mb-0">Specialization: {{ $doctor->specialization }}</p>
                                  <p class="card-text mb-0">Phone: {{ $doctor->phone }}</p>
                                  <p class="card-text mb-0">Email: {{ $doctor->email }}</p>
                                  @php
                                  $leaveType = 'Duty';
                                  if($doctorStatus[$doctor->cid] !== 'Available') {
                                      $latestLeave = $leaves->where('cid', $doctor->cid)->sortByDesc('created_at')->first();
                                      if($latestLeave) {
                                          $leaveType = $latestLeave->type;
                                      }
                                  }
                                  @endphp
                                  <p class="card-text mb-0">Leave Type: {{ $leaveType }}</p>
                              </div>
                          </div>
                      </div>
                      @endforeach
                      @endif
</section>
</main>
</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    function navigateToDetail(url) {
        window.location.href = url;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    // Desktop version
    document.getElementById('searchTypeDesktop').addEventListener('change', function() {
        var selectedType = this.value;
        var cards = document.querySelectorAll('.card');

        cards.forEach(function(card) {
            var leaveType = card.querySelector('.card-body').querySelectorAll('p')[4].textContent.split(':')[1].trim().toLowerCase();
            if(selectedType === '' || selectedType === leaveType || (selectedType === 'duty' && leaveType === 'duty')) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });
    });

    // Mobile version
    document.getElementById('searchTypeMobile').addEventListener('change', function() {
        var selectedType = this.value;
        var cards = document.querySelectorAll('.card');

        cards.forEach(function(card) {
            var leaveType = card.querySelector('.card-body').querySelectorAll('p')[4].textContent.split(':')[1].trim().toLowerCase();
            if(selectedType === '' || selectedType === leaveType || (selectedType === 'duty' && leaveType === 'duty')) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });
    });
</script>
</html>
