<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto  active"><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
        </ul>
      </nav>
    </div>
  </header>

  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800; margin: 50px 0px;">Add Adminstrative Assitant</h4>
    @if(session('success'))
        <div class="alert alert-success" id="successAlert">
            {{ session('success') }}
        </div>
        <script>
            setTimeout(function() {
                $('#successAlert').fadeOut('fast');
            }, 5000);
        </script>
    @endif

    @if(session('error'))
        <div id="error-alert" class="alert alert-danger">
            {{ session('error') }}
        </div>
        <script>
            setTimeout(function() {
                var errorAlert = document.getElementById('error-alert');
                if(errorAlert) {
                    errorAlert.style.display = 'none';
                    }
                }, 5000);
        </script>
    @endif

    <form id="form" method="POST" action="{{ route('addAdm') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label>Name</label>
                <input type="text" name="name" id="name" class="form-control rounded-1">
                <div id="nameError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label>Email address</label>
                <input type="email" name="email" id="email" class="form-control rounded-1">
                <div id="emailError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label>Phone Number</label>
                <input type="text" name="phone" id="phone" class="form-control rounded-1">
                <div id="phoneError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label>CID Number</label>
                <input type="text" name="cid" id="cid" class="form-control rounded-1">
                <div id="cidError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label for="location">Location</label>
                <select name="location" id="location" class="form-control rounded-1">
                    <option value=""></option>
                    <?php
                    $locations = App\Models\Hospitals::pluck('location')->unique();
                    if($locations->count() > 0) {
                        foreach($locations as $location) {
                            echo '<option value="' . $location . '">' . $location . '</option>';
                        }
                    } else {
                        echo '<option disabled>No locations available</option>';
                    }
                    ?>
                </select>
                <div id="locationError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label for="hospitalName">Hospital</label>
                <select name="hospitalName" id="hospitalName" class="form-control rounded-1">
                    <option value=""></option>
                    <?php
                    $hospitals = App\Models\Hospitals::pluck('name');
                    if($hospitals->count() > 0) {
                        foreach($hospitals as $hospital) {
                            echo '<option value="' . $hospital . '">' . $hospital . '</option>';
                        }
                    } else {
                        echo '<option disabled>No hospitals available</option>';
                    }
                    ?>
                </select>
                <div id="hospitalNameError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>
            </div>
        </div>

        <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4" style="font-size: 14px;" />
        <div id="imageError" class="error-message" style="color: red; font-size: 14px;font-weight:600;margin-top:5px"></div>

        <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
            <button class="btn btn-primary" type="submit">Save</button>
            <button class="btn-2" type="button" id="cancelButton">Cancel</button>
        </div>
    </form>

    </div>
</section>
</main>

  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script>
    const form = document.getElementById('form');

    form.addEventListener('submit', (e) => {
        const name = document.getElementById('name');
        const email = document.getElementById('email');
        const phone = document.getElementById('phone');
        const cid = document.getElementById('cid');
        const location = document.getElementById('location');
        const hospitalName = document.getElementById('hospitalName');
        const image = document.getElementById('image');

        const nameError = document.getElementById('nameError');
        const emailError = document.getElementById('emailError');
        const phoneError = document.getElementById('phoneError');
        const cidError = document.getElementById('cidError');
        const locationError = document.getElementById('locationError');
        const hospitalNameError = document.getElementById('hospitalNameError');
        const imageError = document.getElementById('imageError');

        let hasError = false;

        nameError.innerText = '';
        emailError.innerText = '';
        phoneError.innerText = '';
        cidError.innerText = '';
        locationError.innerText = '';
        hospitalNameError.innerText = '';
        imageError.innerText = '';

        if (name.value.trim() === '') {
            nameError.innerText = '* Name is required';
            hasError = true;
        } else if (!name.value.match(/^[a-zA-Z\s]+$/)) {
            nameError.innerText = '* Invalid name';
            hasError = true;
        }

        if (email.value.trim() === '') {
            emailError.innerText = '* Email address is required';
            hasError = true;
        } else if (!isValidEmail(email.value.trim())) {
            emailError.innerText = '* Invalid email address';
            hasError = true;
        }

        if (phone.value.trim() === '') {
            phoneError.innerText = '* Phone number is required';
            hasError = true;
        }

        if (cid.value.trim() === '') {
            cidError.innerText = '* CID number is required';
            hasError = true;
        }

        if (location.value === '') {
            locationError.innerText = '* Location is required';
            hasError = true;
        }

        if (hospitalName.value === '') {
            hospitalNameError.innerText = '* Hospital is required';
            hasError = true;
        }

        if (!image.files.length) {
            imageError.innerText = '* Image is required';
            hasError = true;
        } else if (!isValidImage(image.files[0])) {
            imageError.innerText = '* Invalid image type';
            hasError = true;
        }

        if (hasError) {
            e.preventDefault();
        }
    });

    function isValidEmail(email) {
        const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailPattern.test(email);
    }

    function isValidImage(file) {
        const allowedTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];
        return allowedTypes.includes(file.type);
    }

    document.getElementById('cancelButton').addEventListener('click', () => {
        window.location.href = "{{ url('/admin/viewAdm') }}";
    });
</script>
</body>
</html>
