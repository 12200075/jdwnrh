<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/Hospital.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-4.blade.php') }}">
</head>

<style>
    .pagination {
        display: flex;
        justify-content: center;
        margin-top: 20px;
        list-style: none;
        padding: 0;
    }
    .page-item {
        margin: 0 5px;
    }
    .page-link {
        padding: 10px 15px;
        color: #007bff;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        border-radius: 5px;
    }
    .page-item.active .page-link {
        z-index: 3;
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }
    .page-item.disabled .page-link {
        color: #6c757d;
        pointer-events: none;
        background-color: #fff;
        border-color: #dee2e6;
    }
</style>

<body>
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto  active"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/adminProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>

  <main id="main">
    <section id="hero" class="about">
        <div class="container text-left">
            <h4 style="font-weight: 800;">Hospital List</h4>
            <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 20px;">
                <h4 style="font-weight: 800; margin: 0;">
                    <div class="dropdown" style="font-size: 14px; display: flex; align-items: center; color: rgb(86, 84, 84); margin-top: 10px; padding: 20px;">
                        Select Dzongkhag:
                        <select class="dropdown-toggle" style="margin-left: 20px; font-size: 14px; display: flex; align-items: center; color: rgb(86, 84, 84); padding: 5px" name="location" id="locationSelect">
                            <option>-- Select Location --</option>
                            <?php
                            $locations = App\Models\Hospitals::pluck('location')->unique();

                            if ($locations->count() > 0) {
                                foreach ($locations as $location) {
                                    echo '<option value="' . $location . '">' . $location . '</option>';
                                }
                            } else {
                                echo '<option disabled>No locations available</option>';
                            }
                            ?>
                        </select>
                    </div>
                </h4>

                <div class="d-none d-md-flex justify-content-start align-items-center">
                    <button class="btn btn-primary me-md-2" type="button"><a href="{{ route('addHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Add Hospital</a></button>
                    <button class="btn btn-primary" type="button"><a href="{{ route('downloadHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
                </div>
            </div>

            <div class="d-md-none pr-md-3">
                <button class="btn btn-primary mt-2" type="button"><a href="{{ route('addHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Add Hospital</a></button>
                <button class="btn btn-primary mt-2" type="button"><a href="{{ route('downloadHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
            </div>

            @if(session('success'))
                <div class="alert alert-success" id="successAlert">
                    {{ session('success') }}
                </div>
                <script>
                    setTimeout(function() {
                        $('#successAlert').fadeOut('fast');
                    }, 5000);
                </script>
            @endif

            @if(session('error'))
                <div id="error-alert" class="alert alert-danger">
                    {{ session('error') }}
                </div>
                <script>
                    setTimeout(function() {
                        var errorAlert = document.getElementById('error-alert');
                        if(errorAlert) {
                            errorAlert.style.display = 'none';
                        }
                    }, 5000);
                </script>
            @endif

            <div id="hospitalList">
                @if($hospitals->isNotEmpty())
                    @foreach($hospitals as $hospital)
                        <div class="card col-12 locationCard" data-location="{{ $hospital->location }}">
                            <img src="{{ asset('/storage/images/' . $hospital->image) }}" alt="{{ $hospital->name }}">
                            <div class="card-body" style="display: flex; flex-direction: row; align-items: center;">
                                <p class="card-text">
                                    <b>{{ $hospital->name }}</b><br>
                                    {{ $hospital->location }}<br>
                                </p>
                            </div>
                            <div class="dropdown">
                                <i class="fas fa-ellipsis-h" id="ellipsisToggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                <div class="dropdown-menu" aria-labelledby="ellipsisToggle">
                                    <a class="dropdown-item" href="{{ route('editHospital', ['id' => $hospital->id]) }}" title="Edit Hospital">Edit</a>
                                    <a href="#" class="dropdown-item" title="Delete Hospital" data-toggle="modal" data-target="#deleteHospitalModal">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>No hospitals found.</p>
                @endif
            </div>

            <!-- Pagination Links -->
            {{-- <div class="pagination justify-content-center mt-4">
                @if ($hospitals->onFirstPage())
                    <a class="btn btn-primary" href="#">Previous</a>
                @else
                    <a class="btn btn-primary" href="{{ $hospitals->previousPageUrl() }}">Previous</a>
                @endif

                <span class="mx-2">Page {{ $hospitals->currentPage() }} of {{ $hospitals->lastPage() }}</span>

                @if ($hospitals->hasMorePages())
                    <a class="btn btn-primary" href="{{ $hospitals->nextPageUrl() }}">Next</a>
                @else
                    <a class="btn btn-primary" href="#">Next</a>
                @endif
            </div> --}}

            <!-- Delete Hospital Modal -->
            <div class="modal fade" id="deleteHospitalModal" tabindex="-1" role="dialog" aria-labelledby="deleteHospitalModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteHospitalModalLabel">Confirm Deletion</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to delete this hospital?
                        </div>
                        <div class="modal-footer">
                            <form method="POST" action="{{ route('deleteHospital', ['id' => $hospital->id]) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#locationSelect').change(function() {
                var selectedLocation = $(this).val();
                $('.locationCard').each(function() {
                    var location = $(this).data('location');
                    if (selectedLocation === '-- Select Location --' || location === selectedLocation) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            });
        });
    </script>
</body>

</html>
